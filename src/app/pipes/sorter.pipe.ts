import { Pipe, PipeTransform } from '@angular/core';
import {IncomeOutcome} from '../models/income-outcome';

@Pipe({
  name: 'sorter'
})
export class SorterPipe implements PipeTransform {

  transform(items: IncomeOutcome[]): IncomeOutcome[] {
    return items.sort( (a, b) => {

      if ( a.type === 'income' ) {
        return -1;
      } else {
        return 1;
      }

    });
  }
}
