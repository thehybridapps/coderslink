import { Injectable } from '@angular/core';
import {AngularFireAuth} from '@angular/fire/auth';
import {map} from 'rxjs/operators';
import {User} from '../models/user';
import {AngularFirestore} from '@angular/fire/firestore';
import {Store} from '@ngrx/store';
import {AppState} from '../app.reducer';
import * as authActions from '../auth/auth.actions';
import * as incomeOutcomeActions from '../income-outcome/income-outcome.actions';
import {Subscription} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AuthService  {

  userSubscription: Subscription;
  // tslint:disable-next-line:variable-name
  private _user: User;

  get user() {
    return this._user;
  }

  constructor(public auth: AngularFireAuth, private fireStore: AngularFirestore, private store: Store<AppState>) { }

  initAuthListener(){
    this.auth.authState.subscribe((fuser) => {
      if (fuser) {
        this.userSubscription = this.fireStore.doc(`${fuser.uid}/user`).valueChanges()
          .subscribe((firestoreUser: any) => {
            const tempUser = User.fromFireBase(firestoreUser);
            this._user = tempUser;
            this.store.dispatch(authActions.setUser({user: tempUser}));
          });
      } else {
        this._user = null;
        this.userSubscription.unsubscribe();
        this.store.dispatch(authActions.unSetUser());
        this.store.dispatch( incomeOutcomeActions.unSetItems() );
      }

    });
  }

  createUser(name: string, email: string, password: string ){

   return  this.auth.createUserWithEmailAndPassword(email,  password)
     .then( ({ user }) => {
       const newUser = new User(user.uid, name, user.email);
       return this.fireStore.doc(`${user.uid}/user`).set({...newUser});
     });
  }


  loginUser(email: string, password: string ){
    return  this.auth.signInWithEmailAndPassword(email,  password);

  }

  logout() {
    return this.auth.signOut();
  }

  isAuth() {
    return this.auth.authState.pipe(
      map( fbUser => fbUser !== null)
    );
  }




}
