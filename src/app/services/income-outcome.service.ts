import { Injectable } from '@angular/core';
import {AngularFirestore} from '@angular/fire/firestore';
import {AuthService} from './auth.service';
import {IncomeOutcome} from '../models/income-outcome';
import 'firebase/firestore';

import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class IncomeOutcomeService {


  constructor( private firestore: AngularFirestore,
               private authService: AuthService ) { }

  createIncomeOutcome( incomeOutcome: IncomeOutcome ) {
    const uid = this.authService.user.uid;

    delete incomeOutcome.uid;

    return this.firestore.doc(`${ uid }/income-outcome`)
      .collection('items')
      .add({ ...incomeOutcome });
  }

  initIncomeOutcomeListener(uid: string) {
    return this.firestore.collection(`${ uid }/income-outcome/items`).snapshotChanges()
      .pipe(
        map( snapshot => snapshot.map( (doc: any) => ({
            uid: doc.payload.doc.id,
            ...doc.payload.doc.data() as any
          })
          )
        )
      );
  }
  deleteIncomeOutcome( uidItem: string ) {
    const uid = this.authService.user.uid;
    return this.firestore.doc(`${ uid }/income-outcome/items/${ uidItem }`).delete();

  }

}
