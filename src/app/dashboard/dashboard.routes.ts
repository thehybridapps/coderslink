import {Routes} from '@angular/router';
import {StadisticsComponent} from '../stadistics/stadistics.component';
import {IncomeOutcomeComponent} from '../income-outcome/income-outcome.component';
import {DetailsComponent} from '../details/details.component';


export const dashboardRoutes: Routes = [
  {path: '', component: StadisticsComponent},
  {path: 'income-outcome', component: IncomeOutcomeComponent},
  {path: 'details', component: DetailsComponent},
];
