import {Component, OnDestroy, OnInit} from '@angular/core';
import {Store} from '@ngrx/store';
import {AppState} from '../app.reducer';
import {IncomeOutcomeService} from '../services/income-outcome.service';
import {Subscription} from 'rxjs';
import {filter} from 'rxjs/operators';
import * as incomeOutcomeActions from '../income-outcome/income-outcome.actions';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit, OnDestroy {


  userSubs: Subscription;
  incomeSubs: Subscription;

  constructor(private store: Store<AppState>,
              private incomeOutcomeService: IncomeOutcomeService) { }

  ngOnInit(): void {

    this.userSubs = this.store.select('user')
      .pipe(
        filter( (auth: any) => auth.user !== null )
      )
      .subscribe( ({user}) => {

        console.log(user);
        this.incomeSubs = this.incomeOutcomeService.initIncomeOutcomeListener( user.uid )
          .subscribe( incomeOutcomeFB => {
            console.log(incomeOutcomeFB);
            this.store.dispatch( incomeOutcomeActions.setItems({ items: incomeOutcomeFB }));
          });
      });

  }

  ngOnDestroy(){
    this.incomeSubs.unsubscribe();
    this.userSubs.unsubscribe();
  }

}
