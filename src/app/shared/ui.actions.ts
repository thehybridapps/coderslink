import {createAction} from '@ngrx/store';

// @ts-ignore
export const isLoading = createAction(['[UI Component] Is Loading']);
// @ts-ignore
export const stopLoading = createAction(['[UI Component] Stop Loading']);
