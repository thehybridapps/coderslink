import {Component, OnDestroy, OnInit} from '@angular/core';
import {IncomeOutcome} from '../models/income-outcome';
import {Store} from '@ngrx/store';
import {AppState} from '../app.reducer';
import Swal from 'sweetalert2';
import { Subscription } from 'rxjs';
import {IncomeOutcomeService} from '../services/income-outcome.service';

@Component({
  selector: 'app-details',
  templateUrl: './details.component.html',
  styleUrls: ['./details.component.scss']
})
export class DetailsComponent implements OnInit, OnDestroy {

  incomeOutcome: IncomeOutcome[] = [];
  incomeSubs: Subscription;

  constructor( private store: Store<AppState>,
               private incomeOutcomeService: IncomeOutcomeService ) { }

  ngOnInit() {
    this.incomeSubs = this.store.select('incomeOutcome')
      .subscribe( ({ items }) => this.incomeOutcome = items );
  }
  ngOnDestroy(){
    this.incomeSubs.unsubscribe();
  }

  delete( uid: string ) {
    this.incomeOutcomeService.deleteIncomeOutcome( uid )
      .then( ()   => Swal.fire('Delete', 'Item Deleted' , 'success') )
      .catch( err => Swal.fire('Error', err.message , 'error') );
  }

}
