import { createAction, props } from '@ngrx/store';
import {IncomeOutcome} from '../models/income-outcome';

export const unSetItems = createAction('Unset Items');
export const setItems = createAction('Set Items', props<{ items: IncomeOutcome[] }>()
);


