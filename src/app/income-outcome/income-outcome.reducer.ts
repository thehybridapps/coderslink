import { createReducer, on } from '@ngrx/store';
import {IncomeOutcome} from '../models/income-outcome';
import {setItems, unSetItems} from './income-outcome.actions';

export interface State {
  items: IncomeOutcome[];
}

export const initialState: State = {
  items: [],
}

// tslint:disable-next-line:variable-name
const _incomeOutcomeReducer = createReducer(initialState,

  on( setItems,   (state, { items }) => ({ ...state, items: [...items]  })),
  on( unSetItems, state => ({ ...state, items: []  })),

);

export function incomeOutcomeReducer(state, action) {
  return _incomeOutcomeReducer(state, action);
}
