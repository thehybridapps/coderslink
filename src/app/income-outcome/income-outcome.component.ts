import {Component, OnDestroy, OnInit} from '@angular/core';
import Swal from 'sweetalert2';

import { Store } from '@ngrx/store';
import { AppState } from '../app.reducer';
import * as ui from '../shared/ui.actions';
import { Subscription } from 'rxjs';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {IncomeOutcome} from '../models/income-outcome';
import {IncomeOutcomeService} from '../services/income-outcome.service';

@Component({
  selector: 'app-income-outcome',
  templateUrl: './income-outcome.component.html',
  styleUrls: ['./income-outcome.component.scss']
})
export class IncomeOutcomeComponent implements OnInit, OnDestroy {


  incomeForm: FormGroup;
  type       = 'income';
  loading    = false;
  loadingSubs: Subscription;

  constructor( private fb: FormBuilder,
               private incomeOutcomeService: IncomeOutcomeService,
               private store: Store<AppState>) { }

  ngOnInit() {

    this.loadingSubs = this.store.select('ui')
      .subscribe( ({ isLoading }) => this.loading = isLoading );

    this.incomeForm = this.fb.group({
      description: ['', Validators.required ],
      amount: ['', Validators.required ],
    });

  }

  ngOnDestroy() {
    this.loadingSubs.unsubscribe();
  }

  save() {

    console.log(this.incomeForm.value);

    if ( this.incomeForm.invalid ) { return; }

    this.store.dispatch( ui.isLoading() );

    const { description, amount } = this.incomeForm.value;

    const incomeOutcome = new IncomeOutcome(description, amount, this.type);

    this.incomeOutcomeService.createIncomeOutcome( incomeOutcome )
      .then( () => {
        this.incomeForm.reset();
        this.store.dispatch( ui.stopLoading() );
        Swal.fire('Record Created', description , 'success');
      })
      .catch( err => {
        this.store.dispatch( ui.stopLoading() );
        Swal.fire('Error', err.message , 'error');
      });
  }

}
