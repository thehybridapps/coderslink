import {createAction, props} from '@ngrx/store';
import {User} from '../models/user';

export const setUser = createAction('Set User', props<{user: User}>());

export const unSetUser = createAction('UnSet User');
