// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: 'AIzaSyCiya38Gx0bVIWzWRz5Rhnh0URVFtaiuL0',
    authDomain: 'coderslink-7b060.firebaseapp.com',
    databaseURL: 'https://coderslink-7b060.firebaseio.com',
    projectId: 'coderslink-7b060',
    storageBucket: 'coderslink-7b060.appspot.com',
    messagingSenderId: '471717457719',
    appId: '1:471717457719:web:9df8a607563d259f9834b2',
    measurementId: 'G-VDYEFSXK4V'
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
