export const environment = {
  production: true,
  firebase: {
    apiKey: 'AIzaSyCiya38Gx0bVIWzWRz5Rhnh0URVFtaiuL0',
    authDomain: 'coderslink-7b060.firebaseapp.com',
    databaseURL: 'https://coderslink-7b060.firebaseio.com',
    projectId: 'coderslink-7b060',
    storageBucket: 'coderslink-7b060.appspot.com',
    messagingSenderId: '471717457719',
    appId: '1:471717457719:web:9df8a607563d259f9834b2',
    measurementId: 'G-VDYEFSXK4V'
  }
};
